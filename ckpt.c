#include "helper.h"
#include <assert.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ucontext.h>
#include <unistd.h>

int SaveCkpt() {
  unsigned long long start, end, length = 0;
  int fd_Open, fd_Write, i = 0;
  char str[MAX];
  char ch, *string, *startAddr, *endAddr, *permission, *line;
  ucontext_t mycontext;

  // open the maps file
  fd_Open = open(MAPS_PATH, O_RDONLY);
  fd_Write = open("./myckpt", O_CREAT | O_WRONLY | O_APPEND, S_IRWXU);

  // notifies if any errors
  if (fd_Open < 0) {
    printf("Failed to open maps file.\n");
    exit(EXIT_FAILURE);
  }

  if (fd_Write < 0) {
    printf("Failed to create myckpt file.\n");
    exit(EXIT_FAILURE);
  }

  // write context
  if (getcontext(&mycontext) < 0) {
    printf("Failed to get context.\n");

    return -1;
  }

  if (flag == 1) {

    return 0;

  } else {
    flag = 1;
  }

  res = (int)write(fd_Write, &mycontext, sizeof(ucontext_t));

  if (res <= 0) {
    printf("Failed to save context.\n");
  } else {
    printf("Successfully saved context.\n");
  }

  // read line by line and extract information
  while ((read(fd_Open, &ch, 1)) > 0) {
    if (ch != '\n') {
      *(str + i) = ch;
      i++;

      continue;

    } else if (ch == '\n' || ch == '\0') {
      *(str + i) = '\0';
      i = 0;
    }
    line = strtok(str, "\n");
    line[strlen(line)] = '\0';

    // Skip [vvar], [vdso], [vsyscall]
    if (strstr(line, "[vvar]") == NULL && strstr(line, "[vdso]") == NULL &&
        strstr(line, "[vsyscall]") == NULL) {

      // count how many MemoryRegion(line)
      printf("The line is: %s\n", line);

      // Get startAddr, endAddr, and permission
      mr = (struct MemoryRegion *)malloc(sizeof(struct MemoryRegion));
      startAddr = strtok(line, " ");
      string = strdup(startAddr);
      permission = strtok(NULL, " ");
      startAddr = strtok(string, "-");
      endAddr = strtok(NULL, "\0");

      // Calculates the length of Memory Address
      start = hex_to_ten(startAddr);
      end = hex_to_ten(endAddr);
      length = end - start;
      mr->startAddr = (void *)start;
      mr->endAddr = (void *)end;
      strcpy(mr->permission, permission);

      // print the header information
      printf("\nThe Headers are:\n");
      printf("|%p| |%p| |%s| |%llu|\n", mr->startAddr, mr->endAddr,
             mr->permission, length);
      printf("%ld\n", (char *)mr->endAddr - (char *)mr->startAddr);

      // Saves header and memory data to file
      if (mr->permission[0] == 'r') {
        if ((res = saveToDisk(fd_Write, mr)) != 0) {
          printf("Failed to save checkpoint image.\n");

          return -1;
        } else {

          free(mr);

          continue;
        }
      } else {
        printf("Denied access, skipping.\n");

        free(mr);

        continue;
      }
    }
  }

  printf("---------Successfully saved checkpoint file.--------\n\n");

  close(fd_Open);
  close(fd_Write);

  return 0;
}

// Saving and Restoring registers - save the ucontext
int saveToDisk(int fd_Write, struct MemoryRegion *mr) {
  unsigned long long val1, val2;
  val1 =
      (unsigned long long int)write(fd_Write, mr, sizeof(struct MemoryRegion));

  printf("%llu\n", val1);

  if (val1 <= 0) {
    printf("Failed to save header.\n");

    return -1;

  } else {
    printf("\nSuccessfully saved to header.\n");
  }

  printf("%p %ld\n", mr->startAddr, mr->endAddr - mr->startAddr);
  val2 = (unsigned long long int)write(
      fd_Write, mr->startAddr, (char *)mr->endAddr - (char *)mr->startAddr);
  printf("%llu\n", val2);

  if (val2 <= 0) {
    printf("Failed to save memory data.\n");
    mrCount++;

    return -1;

  } else {
    printf("\nSuccessfully saved to memory data.\n");
  }

  return 0;
}

// Declaring constructor functions
__attribute__((constructor)) void myconstructor() {
  // get pid
  printf("\nThe pid is: %d\n", getpid());
  signal(SIGUSR2, handler);
}

void handler(int sign) {
  signal(sign, SIG_DFL);

  if ((res = SaveCkpt()) != 0) {
    printf("Failed!\n");
  }
}
