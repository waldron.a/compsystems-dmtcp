#ifndef HELPER_H
#define HELPER_H

#define MAPS_PATH "/proc/self/maps"
#define MAX 1000

// Memory region struct usage for ckpt abd my restart files
struct MemoryRegion {
  void *startAddr;
  void *endAddr;
  char permission[4];
};

// Declaring variables
struct MemoryRegion *mr;
int ret;
void *map;

int mrCount;
int flag;
int res;

/*

Declaring all the function definition

*/

// Converts the hexidecimal into a ten base number
unsigned long long hex_to_ten(char *hex);

// Saves the memory region to a certain location
int saveToDisk(int fd_Write, struct MemoryRegion *mr);

// Saves the checkpoint of the memory locations
int SaveCkpt();

// Restores the memory
void restore_memory();

// Handles errors if the memory restore fails
void handler(int sign);

// Declaring constructor functions
__attribute__((constructor)) void myconstructor();

#endif
