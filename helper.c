#include "helper.h"
#include <assert.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ucontext.h>
#include <unistd.h>

// Converts hex string into ten base number
unsigned long long hex_to_ten(char *hex) {
  int i;
  char c;
  unsigned long long value = 0;
  int length = (int)strlen(hex);

  for (i = 0; i < length; i++) {
    c = *(hex + i);
    if ((c >= '0') && (c <= '9'))
      c -= '0';
    else if ((c >= 'a') && (c <= 'f'))
      c -= 'a' - 10;
    else if ((c >= 'A') && (c <= 'F'))
      c -= 'A' - 10;
    value = value * 16 + c;
  }

  return value;
}
