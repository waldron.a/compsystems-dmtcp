#Sample Makefile

CFLAGS=-g -O0 -Wall -Werror

all:	check

default: check

clean-ckpt:
	rm -rf myckpt

clean: clean-ckpt
	rm -rf myrestart hello ckpt ckpt.o libckpt.so hello.o helper.o

hello.o: hello.c
	gcc ${CFLAGS} -c -fpic -o hello.o hello.c

hello:	hello.o
	gcc ${CFLAGS} -g -o hello hello.o

helper.o: helper.c helper.h
	gcc ${CFLAGS} -c -fpic -o helper.o helper.c

ckpt.o: ckpt.c helper.h
	gcc ${CFLAGS} -c -fpic -o ckpt.o ckpt.c

myckpt: ckpt.o
	gcc ${CFLAGS} -g -o myckpt myckpt.o

libckpt.so: ckpt.o helper.o
	gcc ${CFLAGS} -shared -o libckpt.so ckpt.o helper.o

myrestart: myrestart.c helper.h helper.o
	gcc ${CFLAGS} -g -static -Wl,-Ttext-segment=5000000 -Wl,-Tdata=5100000 -Wl,-Tbss=5200000 -o myrestart myrestart.c helper.o

restart: 	myrestart
	./myrestart myckpt -fno-stack-protector


tests:
	cd test $$ gcc -o dmtcp1 dmtcp1.c

checktest: tests
	@ if which id > /dev/null && test "`id -u`" = "0" ; then \
		echo "'make check' will fail if run as root."; \
		exit 1; \
	fi

check1:  tests
	 gcc -o dmtcp1 dmtcp1.c

check2:  tests
	 gcc -o dmtcp2 dmtcp2.c

check3:  tests
	./shared-memory

gdb:
	gdb --args ./myrestart myckpt

check:	clean-ckpt libckpt.so hello myrestart
	(sleep 3 && kill -12 `pgrep -n hello` && sleep 2 && pkill -9 hello) &
	LD_PRELOAD=`pwd`/libckpt.so ./hello
	(sleep 2 &&  pkill -9 myrestart) & make restart

dist:
	dir=`basename $$PWD`; cd ..; tar cvf $$dir.tar ./$$dir; gzip $$dir.tar
