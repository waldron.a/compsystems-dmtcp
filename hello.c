
#include <stdio.h>
#include <unistd.h>

// infinite loop
int main() {
  for (int i = 0;; i++) {
    printf("%d \n", i);
    fflush(stdout);
    sleep(1);
  }

  return 0;
}
