#include "helper.h"
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <ucontext.h>
#include <unistd.h>

#define ADDRESS 0x5300000
#define SIZE 0x1000

char ckpt_image[1000]; // Get the name of file
ucontext_t uc;

// Step1: remove the current stack of program myrestart using ummap
void restore_memory() {
  int fd, i = 0;
  char *line, *address, *startAddr, *endAddr, ch;
  char str[MAX];
  unsigned long long start, end;
  long int length = 0;

  if ((fd = open(MAPS_PATH, O_RDONLY)) < 0) {
    printf("Failed to open self maps file.\n");
    exit(EXIT_FAILURE);
  };

  while ((read(fd, &ch, 1)) > 0) {
    if (ch != '\n') {
      *(str + i) = ch;
      i++;

      continue;

    } else {
      *(str + i) = '\0';
      i = 0;
    }
    line = strtok(str, "\n");
    line[strlen(line)] = '\0';

    // Part d - Unmap the stack
    // d.a - find the stack memory section and munmap it
    if ((strstr(line, "[stack]")) != NULL) {
      address = strtok(line, " ");
      startAddr = strtok(address, "-");
      endAddr = strtok(NULL, "\0");
      start = hex_to_ten(startAddr);
      end = hex_to_ten(endAddr);
      length = (long)end - start;

      // d.c - Call mumap on the located stack
      munmap((void *)startAddr, (size_t)length);
      printf("unmap Successful!\n\n");
    }
  }
  close(fd);

  // Step 2: restore the memory data and context from the checkpoint file.
  if ((fd = open(ckpt_image, O_RDONLY)) < 0) {
    printf("Failed to open checkpoint file.\n");
    exit(EXIT_FAILURE);
  };

  // Read the context first and store it into uc
  if ((read(fd, &uc, sizeof(ucontext_t))) <= 0) {
    printf("Failed to read context from checkpoint file.\n");
  }

  mr = (struct MemoryRegion *)malloc(sizeof(struct MemoryRegion));

  while ((read(fd, mr, sizeof(struct MemoryRegion))) > 0) {

    // Part 2.a - mmap the memory section for a new stack
    length = (char *)mr->endAddr - (char *)mr->startAddr;
    printf("startAddr:%p length: %ld\n", mr->startAddr, length);

    // Part 2.b - copy filename from stack, into a data segment
    if ((map = mmap(mr->startAddr, (size_t)length,
                    PROT_READ | PROT_EXEC | PROT_WRITE,
                    MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0)) < 0) {
      printf("Failed to mmap memory section. Error: %s\n", strerror(errno));
    } else {
      printf("mmap memory section was Successful!\n\n");
    }

    // Restore the memory data
    mprotect(mr->startAddr, (size_t)length, PROT_WRITE);
    printf("startAddr:%p length: %ld\n", mr->startAddr,
           (char *)mr->endAddr - (char *)mr->startAddr);

    if (read(fd, mr->startAddr, (char *)mr->endAddr - (char *)mr->startAddr) <
        0) {
      printf("Failed to read memory data. Error: %s\n", strerror(errno));
    } else {
      printf("\nSuccessfully read memory section data!\n");
    }

    // Setting the permission
    int permissions = 0;
    if (mr->permission[0] != '-')
      permissions |= PROT_READ;
    if (mr->permission[1] != '-')
      permissions |= PROT_WRITE;
    if (mr->permission[2] != '-')
      permissions |= PROT_EXEC;
    if ((ret = mprotect(mr->startAddr, (size_t)length, permissions)) < 0)
      printf("Failed to mprotect. Error: %s\n", strerror(errno));
    else
      printf("\nmprotect memory section implementation Successful!\n");

    free(mr);
    mr = (struct MemoryRegion *)malloc(sizeof(struct MemoryRegion));
  }

  printf("\nSuccessfully restored all memory data!\n");
  printf("-----DMTCP program completed!-----\n\n");

  free(mr);

  // Part 2.h: restore the context
  setcontext(&uc);
  close(fd);
}

// Main function to print the checkpoint restored
int main(int argc, char *argv[]) {
  if (argc == 1) {
    printf("Please add file name!\n");

    return -1;
  }

  strcpy(ckpt_image, argv[1]);

  // Map in some new memort for a new stack
  void *p_map;
  p_map = mmap((void *)ADDRESS, SIZE, PROT_READ | PROT_WRITE | PROT_EXEC,
               MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  void *stack_ptr;
  stack_ptr = (void *)(p_map + 0x1000);

  /*
  Step 2.c - Use the inline assembly syntax of gcc to include code that
  will switch the stack pointer.
  */
  asm volatile("mov %0,%%rsp" : : "g"(stack_ptr) : "memory");
  restore_memory();
  printf("Restore Successful!\n");

  return 0;
}
