# CompSystems-DMTCP

# Building and Testing

**Build**
  Use 'make clean' : to clear the whole
  Use 'make gdb' : to debug

**Run:**
  Use 'make' : to run the first step of program
  Use 'make' res : to run the second step of program - restores checkpoint
  
**Test:**
  Enter $ 'dmtcp_coordinator'
	l : for client list
	s : for checkpoint status
	c : checkpoint all nodes
	f : force restart (debugging)
	k : kill nodes
	q : kill all nodes then quit
  Open another terminal/ tab; 
		Enter $  'dmtcp_checkpoint test/dmtcp1'

